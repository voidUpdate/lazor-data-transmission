import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.OUT)

delay = 0.05

def encode(inp):
        #manchester encoding of a binary string
        ret = "11" #packet preamble
        for character in inp:
                if character == "0":
                        ret += "01"
                elif character == "1":
                        ret += "10"
        return str(ret)

def text2bin(inp):
        return ' '.join('{0:08b}'.format(ord(x), 'b') for x in inp)


while True:
        inp = raw_input("awaiting input: ")
        for character in encode(text2bin(inp)):
                GPIO.output(4, character == "1")
                time.sleep(delay)
        GPIO.output(4, False)