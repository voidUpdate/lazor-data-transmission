bool val = false;
bool lastVal = false;

bool reading = false;

int delayTime = 50;

String packet = "";

void setup() {
  Serial.begin(9600);
}

void loop(){
  
  if(reading){//if we are 
     if(analogRead(A0) > 300){
      val = true;
     }
    else{
      val = false;
    }
    delay(delayTime);
    if(analogRead(A0) > 300){
      lastVal = true;
     }
    else{
      lastVal = false;
    }
    
    if(val && !lastVal){
      packet += "1";
    }
    else if(!val && lastVal){
      packet += "0";
    }
    else if(!val && !lastVal){
      Serial.println("END PACKET");
      reading = false; 
      Parse(packet);    
      packet = ""; 
    }
  }
  
  else
  {
    if(analogRead(A0) > 300){
      val = true;
    }
    else{
      val = false;
    }

    //if we got 11, then packet start
    if(val && lastVal){
      Serial.println("RECIEVING PACKET");
      reading = true;
    }
    lastVal = val; 
  }
       
  delay(delayTime);  
}


/********PARSE THE PACKET***********/
void Parse(String packet)
{
  for(int i = 0; i < packet.length(); i += 8){
    bin2ascii(packet.substring(i, i+8));
  }
  Serial.println(" ");
}

void bin2ascii(String packet){
  if(packet.length() != 8){
    Serial.println("Packet size incorrect, skipping");
    return;
  }

  if(packet == "01100001"){
    Serial.print("a");
  }
  else if(packet == "01100010"){
    Serial.print("b");
  }
  else if(packet == "01100011"){
    Serial.print("c");
  }
  else if(packet == "01100100"){
    Serial.print("d");
  }
  else if(packet == "01100101"){
    Serial.print("e");
  }
  else if(packet == "01100110"){
    Serial.print("f");
  }
  else if(packet == "01100111"){
    Serial.print("g");
  }
  else if(packet == "01101000"){
    Serial.print("h");
  }
  else if(packet == "01101001"){
    Serial.print("i");
  }
  else if(packet == "01101010"){
    Serial.print("j");
  }
  else if(packet == "01101011"){
    Serial.print("k");
  }
  else if(packet == "01101100"){
    Serial.print("l");
  }
  else if(packet == "01101101"){
    Serial.print("m");
  }
  else if(packet == "01101110"){
    Serial.print("n");
  }
  else if(packet == "01101111"){
    Serial.print("o");
  }
  else if(packet == "01110000"){
    Serial.print("p");
  }
  else if(packet == "01110001"){
    Serial.print("q");
  }
  else if(packet == "01110010"){
    Serial.print("r");
  }
  else if(packet == "01110011"){
    Serial.print("s");
  }
  else if(packet == "01110100"){
    Serial.print("t");
  }
  else if(packet == "01110101"){
    Serial.print("u");
  }
  else if(packet == "01110110"){
    Serial.print("v");
  }
  else if(packet == "01110111"){
    Serial.print("w");
  }
  else if(packet == "01111000"){
    Serial.print("x");
  }
  else if(packet == "01111001"){
    Serial.print("y");
  }
  else if(packet == "01111010"){
    Serial.print("z");
  }
  else if(packet == "01000001"){
    Serial.print("A");
  }
  else if(packet == "01000010"){
    Serial.print("B");
  }
  else if(packet == "01000011"){
    Serial.print("C");
  }
  else if(packet == "01000100"){
    Serial.print("D");
  }
  else if(packet == "01000101"){
    Serial.print("E");
  }
  else if(packet == "01000110"){
    Serial.print("F");
  }
  else if(packet == "01000111"){
    Serial.print("G");
  }
  else if(packet == "01001000"){
    Serial.print("H");
  }
  else if(packet == "01001001"){
    Serial.print("I");
  }
  else if(packet == "01001010"){
    Serial.print("J");
  }
  else if(packet == "01001011"){
    Serial.print("K");
  }
  else if(packet == "01001100"){
    Serial.print("L");
  }
  else if(packet == "01001101"){
    Serial.print("M");
  }
  else if(packet == "01001110"){
    Serial.print("N");
  }
  else if(packet == "01001111"){
    Serial.print("O");
  }
  else if(packet == "01010000"){
    Serial.print("P");
  }
  else if(packet == "01010001"){
    Serial.print("Q");
  }
  else if(packet == "01010010"){
    Serial.print("R");
  }
  else if(packet == "01010011"){
    Serial.print("S");
  }
  else if(packet == "01010100"){
    Serial.print("T");
  }
  else if(packet == "01010101"){
    Serial.print("U");
  }
  else if(packet == "01010110"){
    Serial.print("V");
  }
  else if(packet == "01010111"){
    Serial.print("W");
  }
  else if(packet == "01011000"){
    Serial.print("X");
  }
  else if(packet == "01011001"){
    Serial.print("Y");
  }
  else if(packet == "01011010"){
    Serial.print("Z");
  }
  else if(packet == "00110000"){
    Serial.print("0");
  }
  else if(packet == "00110001"){
    Serial.print("1");
  }
  else if(packet == "00110010"){
    Serial.print("2");
  }
  else if(packet == "00110011"){
    Serial.print("3");
  }
  else if(packet == "00110100"){
    Serial.print("4");
  }
  else if(packet == "00110101"){
    Serial.print("5");
  }
  else if(packet == "00110110"){
    Serial.print("6");
  }
  else if(packet == "00110111"){
    Serial.print("7");
  }
  else if(packet == "00111000"){
    Serial.print("8");
  }
  else if(packet == "00111001"){
    Serial.print("9");
  }
  else if(packet == "00100000"){
    Serial.print(" ");
  }
  else if(packet == "00100001"){
    Serial.print("!");
  }
  else if(packet == "00101100"){
    Serial.print(",");
  }
  else if(packet == "00101110"){
    Serial.print(".");
  }
  
  
}
